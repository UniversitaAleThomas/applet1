package asw1024.applet;

import asw1024.commons.PlacesTableModel;
import asw1024.commons.RatingListener;
import asw1024.model.response.PlaceResponse;
import asw1024.utils.AppletConstants;
import asw1024.utils.HTTPClient;
import asw1024.utils.ManageXML;
import asw1024.utils.AppletUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.w3c.dom.Document;

/**
 * Applet per la gestione dei locali utente
 *
 * @author Alessandro Neri
 * @author Thomas Trapanese
 */
public class RatingApplet extends JApplet {

    private static final Logger log = Logger.getLogger(RatingApplet.class.getName());
    private HTTPClient httpClient;
    private List<PlaceResponse> userPlaces;
    private int selectedPlaceIndex = -1;
    private JTable tblLocali;
    private JTextArea txtDesLocale;
    private JLabel lblImmagine;
    private RatingListener listenerThread;

    @Override
    public void init() {
        httpClient = new HTTPClient();

        try {
            //Di default sarebbe: Code Base: http://localhost:8084/WebApplication/applet/
            //httpClient.setBase(new URL(getCodeBase().toURI().resolve("..").toString()));

            log.log(Level.INFO, "Code Base: " + getCodeBase().toString());
            log.log(Level.INFO, "Session ID: " + getParameter("sessionId"));

            httpClient.setBase(new URL(getCodeBase().toURI().toString()));
            httpClient.setSessionId(getParameter("sessionId"));

            //Disegno la gui ed inizializzo il modello dei dati
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    initializeGUI();
                }
            });
            getDataModel();

        } catch (Exception e) {
            log.log(Level.SEVERE, "Errore durante lo startup dell'applet", e);
        }
    }

    @Override
    public void start() {
        if (listenerThread == null || listenerThread.isStopped()) {
            listenerThread = new RatingListener(httpClient, userPlaces, (PlacesTableModel) tblLocali.getModel());
            listenerThread.start();
        }
    }

    @Override
    public void stop() {
        listenerThread.stopListening();
    }

    /**
     * Crea l'interfaccia grafica e imposta gli event listener sui relativi
     * componenti swing
     */
    private void initializeGUI() {
        Container cp = getContentPane();
        Color clrWhiteSmoke = new Color(245, 245, 245); //whitesmoke come il tema web

        JPanel mainPanel = new JPanel();
        JPanel pnlTable = new JPanel();
        JPanel pnlDescLocale = new JPanel();
        JPanel pnlButtons = new JPanel();
        JPanel imagePanel = new JPanel();

        cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        pnlTable.setLayout(new BoxLayout(pnlTable, BoxLayout.X_AXIS));
        imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.LINE_AXIS));

        mainPanel.setBackground(clrWhiteSmoke);

        tblLocali = new JTable();
        tblLocali.setBackground(clrWhiteSmoke);
        tblLocali.setPreferredScrollableViewportSize(new Dimension(500, 70));
        tblLocali.setFillsViewportHeight(true);
        tblLocali.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblLocali.setPreferredSize(new Dimension(100, 300));

        pnlTable.add(new JScrollPane(tblLocali));

        mainPanel.add(pnlTable);

        txtDesLocale = new JTextArea(5, 40);
        txtDesLocale.setLineWrap(true);
        txtDesLocale.setWrapStyleWord(true);

        pnlDescLocale.setBorder(BorderFactory.createEtchedBorder());
        pnlDescLocale.setLayout(new BoxLayout(pnlDescLocale, BoxLayout.LINE_AXIS));
        pnlDescLocale.add(new JScrollPane(txtDesLocale));

        mainPanel.add(pnlDescLocale);

        lblImmagine = new JLabel();
        imagePanel.add(lblImmagine);

        mainPanel.add(imagePanel);

        JButton btnAggiungi = new JButton("Aggiungi");
        JButton btnRimuovi = new JButton("Rimuovi");
        JButton btnSalva = new JButton("Salva");

        pnlButtons.setBackground(clrWhiteSmoke);

        pnlButtons.add(btnAggiungi);
        pnlButtons.add(btnRimuovi);
        pnlButtons.add(btnSalva);

        mainPanel.add(pnlButtons);

        getContentPane().add(mainPanel);

        //----------------------------------------
        //Attaching listeners
        btnAggiungi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAggiungiHandler();
            }
        });

        btnRimuovi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRimuoviHandler();
            }
        });

        btnSalva.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSalvaHandler();
            }
        });

        tblLocali.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent event) {
                        selectedTableRowChangedHandler(event);
                    }
                }
        );

        txtDesLocale.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                //Nothing to do
            }

            @Override
            public void focusLost(FocusEvent e) {
                txtDescriptionChangedHandler();
            }
        });

    }

    /**
     * Richiede al server il modello dei dati dell'utente e lo imposta nella
     * JTable tblLocali
     */
    private void getDataModel() {
        //----------------------------------------
        //richiedo l'elenco dei locali dell'utente loggato
        userPlaces = getLoggedUserPlaces();

        //Delego al thread della GUI gli update grafici
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                //imposto il modello dei locali alla table
                tblLocali.setModel(new PlacesTableModel(userPlaces));
                //segnalo alla table la variazione del modello dati
                ((PlacesTableModel) tblLocali.getModel()).fireTableDataChanged();
                //resize delle colonne
                resizeColumnWidth(tblLocali);
            }
        });
    }

    /**
     * Invia al server una richiesta di salvataggio delle modifiche effettuate
     * dall'utente
     */
    private void btnSalvaHandler() {
        if (saveUserPlaces()) {
            JOptionPane.showMessageDialog(getContentPane(), "Tutte le modifiche solo state salvate correttamente");
        } else {
            JOptionPane.showMessageDialog(getContentPane(), "Errore durante il salvataggio dei locali");
        }
    }

    /**
     * Rimuove dal modello dati il {@link PlaceResponse locale} correntemente
     * selezionato
     */
    private void btnRimuoviHandler() {
        if (selectedPlaceIndex == -1) {
            JOptionPane.showMessageDialog(getContentPane(), "Seleziona un locale da rimuovere..");
            return;
        }
        int action = JOptionPane.showConfirmDialog(getContentPane(),
                "Sei sicuro di voler rimuovere il locale?",
                "Rimozione locale", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (action == JOptionPane.YES_OPTION) {
            //Elimino dal modello
            userPlaces.remove(selectedPlaceIndex);
            //Notifico alla JTable il cambiamento del model
            ((PlacesTableModel) tblLocali.getModel()).fireTableRowsDeleted(selectedPlaceIndex, selectedPlaceIndex);
        }
    }

    /**
     * Inserisce un nuovo {@link PlaceResponse locale} al modello dei locali
     */
    private void btnAggiungiHandler() {
        //Aggiungo un nuovo locale al modello
        PlaceResponse pr = new PlaceResponse();
        pr.setId(AppletConstants.ID_NEW_PLACE);
        pr.setName("");
        pr.setDescription("");
        pr.setAddress("");
        pr.setImagePath("");
        userPlaces.add(pr);
        //Segnalo alla table l'aggiunta di una row
        ((PlacesTableModel) tblLocali.getModel()).fireTableRowsInserted(userPlaces.size() - 1, userPlaces.size() - 1);
    }

    /**
     * Controlla se il nuovo valore della {@link JTextArea descrizione} è
     * diverso da quello che aveva precedentemente. In caso affermativo,
     * aggiorna la descrizione del locale selezionato
     */
    private void txtDescriptionChangedHandler() {
        if (selectedPlaceIndex < 0) {
            return;
        }
        PlaceResponse currentPlace = userPlaces.get(selectedPlaceIndex);
        if (!txtDesLocale.getText().equals(currentPlace.getDescription())) {
            //Se il contenuto è stato modificato
            currentPlace.setDescription(txtDesLocale.getText());
        }
    }

    /**
     * Funziona che si occupa di recuperare il nuovo locale selezionato e di
     * aggiornare la GUI di conseguenza
     *
     * @param event riferimento al {@link ListSelectionEvent} della JTable
     */
    private void selectedTableRowChangedHandler(ListSelectionEvent event) {
        if (event.getValueIsAdjusting()) {
            //Il value changed procca su focusLost e focusGained -> controlliamo i valori
            //solo alla fine della transizione
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) event.getSource();

        if (lsm.isSelectionEmpty()) {
            selectedPlaceIndex = -1;
        } else {
            selectedPlaceIndex = tblLocali.getSelectedRow();
        }

        try {
            //Delego al thread della GUI gli update grafici
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    updateGUI(selectedPlaceIndex);
                }
            });
        } catch (Exception e) {
            log.log(Level.SEVERE, "Errore durante il refresh della GUI", e);
        }
    }

    /**
     * Handler per il refresh dell'interfaccia grafica Aggiorna i componenti
     * swing con le informazioni ricavate dal locale attualmente selezionato
     *
     * @param currentSelectedRowIndex indice del locale all'interno del modello
     * dei dati
     */
    private void updateGUI(int currentSelectedRowIndex) {
        if (currentSelectedRowIndex > -1) {
            //E' stata selezionata una nuova riga nella JTable -> aggiorno la GUI
            PlaceResponse currentPlace = userPlaces.get(currentSelectedRowIndex);

            if (currentPlace == null) {
                //Si è selezionato un locale non presente nel modello... 
                //Questa situazione non dovrebbe capitare -> si potrebbe lanciare un Exception
                return;
            }

            //Desc
            txtDesLocale.setText(currentPlace.getDescription());

            //Aggiorno l'immagine del locale, se presente
            if (currentPlace.getImagePath() != null && !currentPlace.getImagePath().isEmpty()) {
                try {
                    BufferedImage orig = ImageIO.read(new URL(currentPlace.getImagePath()));
                    lblImmagine.setIcon(new ImageIcon(orig.getScaledInstance(txtDesLocale.getWidth() / 3, -1, Image.SCALE_FAST)));
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Errore durante il rendering dell'immagine scaricata", e);
                }
            }

        } else {
            //Nella JTable non c'è nessuna riga selezionata -> pulisco la GUI
            txtDesLocale.setText("");
            lblImmagine.setIcon(null);
        }
    }

    /**
     * Invia alla servlet la richiesta di recupero dei locali dell'utente
     * loggato
     *
     * @return L'elenco dei locali dell'utente
     */
    private List<PlaceResponse> getLoggedUserPlaces() {
        try {

            Document docrequest = new ManageXML().newDocument();
            AppletUtils.generateRequestDocument(docrequest, AppletConstants.OP_GET_USR_PLACES);

            //Debug
            AppletUtils.prettyPrintDocument(docrequest, System.out);

            Document docresponse = httpClient.execute(AppletConstants.APPLET_SERVLET_ENDPOINT, docrequest);

            //Debug
            AppletUtils.prettyPrintDocument(docresponse, System.out);

            if (AppletUtils.isAppletResponse(docresponse) && AppletConstants.XML_MESSAGE_OK.equals(AppletUtils.getResponseMessageFromDocument(docresponse))) {
                //Si tratta di un applet response
                return AppletUtils.getUserPlacesFromDocument(docresponse);
            } else {
                //Mostro il messaggio di errore proveniente dalla servlet
                log.log(Level.SEVERE, AppletUtils.getResponseMessageFromDocument(docresponse));
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, "Errore durante il recupero dei locali utente", e);
        }

        return null;
    }

    /**
     * Invia alla servlet la richiesta di salvataggio dei locali modificati
     * dall'utente
     *
     * @return <code>true</code> nel caso la richiesta vada a buon fine,
     * <code>false</code> altrimenti
     */
    private boolean saveUserPlaces() {
        try {

            Document docrequest = new ManageXML().newDocument();
            AppletUtils.generateSavePlacesRequestDocument(docrequest, userPlaces);
            AppletUtils.prettyPrintDocument(docrequest, System.out);

            Document docresponse = httpClient.execute(AppletConstants.APPLET_SERVLET_ENDPOINT, docrequest);

            //Debug
            AppletUtils.prettyPrintDocument(docresponse, System.out);

            if (AppletUtils.isAppletResponse(docresponse) && AppletConstants.XML_MESSAGE_OK.equals(AppletUtils.getResponseMessageFromDocument(docresponse))) {
                //La servlet ha salvato correttamente: leggo la lista dei locali aggiornata
                userPlaces.clear();
                userPlaces.addAll(AppletUtils.getUserPlacesFromDocument(docresponse));

                //segnalo un evento di data changed a tutti i listener sul table model
                ((PlacesTableModel) tblLocali.getModel()).fireTableDataChanged();

                return true;
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, "Errore durante il salvataggio dei locali dell'utente", e);
        }

        return false;
    }

    /**
     * Data una {@link JTable} ne auto dimensiona la larghezza delle colonne
     *
     * @param table riferimento alla JTable di cui ridimensionare le colonne
     */
    public void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 60; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width, width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    @Override
    public void destroy() {
        log.log(Level.INFO, "Applet Destroyed");
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }

}
