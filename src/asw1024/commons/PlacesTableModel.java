/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asw1024.commons;

import asw1024.model.response.PlaceResponse;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Table Model per la struttura dati dei {@link PlaceResponse locali}
 * Consente di rappresentare sulla griglia i dati principali dei locali dell'utente
 * @author Alessandro Neri
 * @author Thomas Trapanese
 */
public class PlacesTableModel extends AbstractTableModel {

    /**
     * Costante che identifica l'indice di colonna del nome del locale
     */
    public static final int NAME_INDEX = 0;
    
    /**
     * Costante che identifica l'indice di colonna dell'indirizzo del locale
     */
    public static final int INDIRIZZO_INDEX = 1;
    
    /**
     * Costante che identifica l'indice di colonna dei voti positivi del locale
     */
    public static final int POS_RATING_INDEX = 2;
        
    /**
     * Costante che identifica l'indice di colonna dei voti negativi del locale
     */
    public static final int NEG_RATING_INDEX = 3;
        
    /**
     * Costante che identifica l'indice di colonna dell'immagine del locale
     */
    public static final int IMG_URL_INDEX = 4;
    
    /**
     * Costante che identifica l'elenco delle colonne editabili dall'utente
     */
    public static final List<Integer> EDITABLE_INDEX;

    //Definisco le colonne editabili dall'utente
    static {
        EDITABLE_INDEX = new ArrayList<>();
        EDITABLE_INDEX.add(NAME_INDEX);
        EDITABLE_INDEX.add(INDIRIZZO_INDEX);
        EDITABLE_INDEX.add(IMG_URL_INDEX);
    }

    protected String[] columnNames;
    protected List<PlaceResponse> dataVector;

    /**
     * Costruttore per <code>PlacesTableModel</code>
     * @param model Modello dati per la JTable, rappresenta un elenco di locali utente
     */
    public PlacesTableModel(List<PlaceResponse> model) {
        super();
        this.columnNames = new String[]{"Denominazione", "Indirizzo", "Voti Positivi", "Voti Negativi", "URL Foto Profilo"};
        if (model != null) {
            dataVector = model;
        } else {
            dataVector = new ArrayList<>();
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return EDITABLE_INDEX.contains(column);
    }

    @Override
    public Class getColumnClass(int column) {
        switch (column) {
            case NAME_INDEX:
            case INDIRIZZO_INDEX:
            case IMG_URL_INDEX:
                return String.class;
            default:
                return Object.class;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        PlaceResponse place = (PlaceResponse) dataVector.get(row);

        switch (column) {
            case NAME_INDEX:
                return place.getName();
            case INDIRIZZO_INDEX:
                return place.getAddress();
            case POS_RATING_INDEX:
                return place.getPositiveRatings();
            case NEG_RATING_INDEX:
                return place.getNegativeRatings();
            case IMG_URL_INDEX:
                return place.getImagePath();
            default:
                return new Object();
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        PlaceResponse place = (PlaceResponse) dataVector.get(row);
        switch (column) {
            case NAME_INDEX:
                place.setName((String) value);
                break;
            case INDIRIZZO_INDEX:
                place.setAddress((String) value);
                break;
            case IMG_URL_INDEX:
                place.setImagePath((String) value);
                break;
            default:
                System.out.println("invalid index");
        }
        fireTableCellUpdated(row, column);
    }

    @Override
    public int getRowCount() {
        return dataVector.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
}
