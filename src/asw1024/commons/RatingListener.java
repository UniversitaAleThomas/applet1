/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asw1024.commons;

import asw1024.model.response.PlaceResponse;
import asw1024.utils.AppletConstants;
import asw1024.utils.HTTPClient;
import asw1024.utils.ManageXML;
import asw1024.utils.AppletUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.w3c.dom.Document;

/**
 * Thread per il monitoring dei voti sui locali dell'utente
 *
 * @author Alessandro Neri
 * @author Thomas Trapanese
 */
public class RatingListener extends Thread {

    private static final Logger log = Logger.getLogger(RatingListener.class.getName());
    private boolean isPolling = true;
    private HTTPClient c;
    private List<PlaceResponse> userPlacesModel;

    private PlacesTableModel tableModel;

    /**
     * Costruttore per <code>RatingListener</code>
     *
     * @param c rifermento all'HTTPClient tramite cui comunicare col server
     * @param userPlacesModel riferimento al modello dati su cui riportare
     * l'update dei voti
     */
    public RatingListener(HTTPClient c, List<PlaceResponse> userPlacesModel, PlacesTableModel model) {
        this.c = c;
        this.userPlacesModel = userPlacesModel;
        this.tableModel = model;
    }

    @Override
    public void run() {
        try {
            ManageXML mngXML = new ManageXML();

            log.log(Level.INFO, "[Rating Listener] In ascolto sui voti");

            while (isPolling) {
                //invia la richiesta di registrazione come rating listener
                Document docrequest = mngXML.newDocument();
                AppletUtils.generateRequestDocument(docrequest, AppletConstants.OP_REGISTER_AS_RATING_LISTENER);

                Document docresponse = c.execute(AppletConstants.APPLET_SERVLET_ENDPOINT, docrequest);
                AppletUtils.prettyPrintDocument(docresponse, System.out);

                if (!AppletConstants.XML_MESSAGE_TIMEOUT.equals(AppletUtils.getResponseMessageFromDocument(docresponse))) {
                    //E' arrivato un aggiornamento di voto

                    List<PlaceResponse> modifiche = AppletUtils.getUserPlacesFromDocument(docresponse);
                    //Sappiamo che il server notifica un solo cambio di voto alla volta
                    if (modifiche != null && modifiche.size() == 1) {
                        final PlaceResponse newVoto = modifiche.get(0);
                        //Ricerchiamo l'aggiornamento nel modello dell'applet
                        for (final PlaceResponse orig : userPlacesModel) {
                            if (orig.getId() == newVoto.getId()) {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        //locale modificato!! aggiorno il modello dati
                                        orig.setPositiveRatings(newVoto.getPositiveRatings());
                                        orig.setNegativeRatings(newVoto.getNegativeRatings());
                                        tableModel.fireTableDataChanged();
                                    }
                                });

                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, "[Rating Listener] Errore durante l'ascolto delle notifiche dei voti", e);
        }
    }

    /**
     * Comunica al thread di interrompere il listening sulle variazione dei voti
     */
    public void stopListening() {
        log.log(Level.INFO, "[Rating Listener] Arrivata richiesta di stop");
        isPolling = false;
    }

    public boolean isStopped() {
        return !isPolling;
    }

}
